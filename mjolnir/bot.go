package mjolnir

import (
	"os"
	"time"

	"github.com/whyrusleeping/hellabot"
	bolt "go.etcd.io/bbolt"
	log "gopkg.in/inconshreveable/log15.v2"
)

type Bot struct {
	Bot    *hbot.Bot
	DB     *bolt.DB
	Config Config
}

type Config struct {
	NickservPass string
	OperUser     string
	OperPass     string
}

var b Bot = Bot{}

func Run(server string, nick string, channels []string, ssl bool, config Config) {
	chans := func(bot *hbot.Bot) {
		bot.Channels = channels
	}
	sslOptions := func(bot *hbot.Bot) {
		bot.SSL = ssl
	}

	b.Config = config

	irc, err := hbot.NewBot(server, nick, chans, sslOptions)
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}

	db, err := newDB()
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}

	b.Bot = irc
	b.DB = db

	// Prepare buckets
	if err := b.DB.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("repeats"))
		return err
	}); err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}

	// Run DB cleanup jobs
	go reapKeys(b.DB, time.Second*10)

	// Triggers to run
	b.Bot.AddTrigger(OperLogin)
	b.Bot.AddTrigger(MessageTrackTrigger)
	b.Bot.AddTrigger(RepeatMessageTrigger)
	b.Bot.AddTrigger(InfoTrigger)
	b.Bot.AddTrigger(NickFloodTrigger)
	b.Bot.Logger.SetHandler(log.StreamHandler(os.Stdout, log.JsonFormat()))

	// GOOOOOOO
	defer b.DB.Close()
	b.Bot.Run()
}
