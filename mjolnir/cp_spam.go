package mjolnir

import (
	"fmt"
	"github.com/whyrusleeping/hellabot"
	"regexp"
)

var NickFloodTrigger = hbot.Trigger{
	func(irc *hbot.Bot, m *hbot.Message) bool {
		match, _ := regexp.MatchString(`Client connecting:\s([A-Z\d]{6})`, m.Trailing)
		return match
	},
	func(irc *hbot.Bot, m *hbot.Message) bool {
		re := regexp.MustCompile(`Client connecting:\s([A-Z\d]{6})`)
		match := re.FindStringSubmatch(m.Trailing)
		irc.Send(fmt.Sprintf("KILL %s spam", match[len(match)-1]))
		return true
	},
}
