#! /bin/sh

# Get latest commit, tag, and branch we build on
export GIT_COMMIT=$(git rev-list -1 HEAD) 
export GIT_TAG=$(git describe --abbrev=0 --tags)
export GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD) 

# We don't want empty tags overriding the default value
if [ ! -z $GIT_TAG ]; then
	go install -ldflags "-X gitlab.com/secops-space/mjolnir/mjolnir.GitCommit=$GIT_COMMIT -X gitlab.com/secops-space/mjolnir/mjolnir.GitVersion=$GIT_TAG -X gitlab.com/secops-space/mjolnir/mjolnir.GitBranch=$GIT_BRANCH"
else
	go install -ldflags "-X gitlab.com/secops-space/mjolnir.GitCommit=$GIT_COMMIT -X gitlab.com/secops-space/mjolnir.GitBranch=$GIT_BRANCH" 
fi

echo "Compiled Mjolnir:\n\tGit tag: $GIT_TAG\n\tGit commit: $GIT_COMMIT\n\tGit branch: $GIT_BRANCH\n"
