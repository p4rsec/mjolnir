# Mjolnir

Spam detection and response for UnrealIRCd and Anope

## Installation and Setup

`go get gitlab.com/secops-space/mjolnir && go install gitlab.com/secops-space/mjolnir`

In order for Mjolnir to take action against spam, it needs a local oper on a server. On UnrealIRCd, a typical Oper block looks like this:

```
oper mjolnir {
        class opers;
        mask *@*;
        password "correct-horse-battery-staple";
        operclass netadmin-with-override;
        swhois "is a Network Administrator";
        vhost mjolnir.secops.space;
};
```

You can create your own oper class tailored for mjolnir as you see fit. Currently it requires permission to see connect/disconnect strings and issue kill, kick, and KLINE commands.

### Config file

The following is an example cofiguration file for Mjolnir. Fields should be self-explanatory

```yaml
server: "irc.secops.space:6697"
nickServ: "your-nickserv-password"
operUser: "mjolnir"
operPass: "correct-horse-battery-staple"
channels:
  - "#main"
nick: "mjolnir"
ssl: true
```

Currently Mjolnir enforces rules uniformly across all channels which it has joined. In other words, there is no "ignore" concept or granulated filtering for channels at this time. Patches are welcome.

## Running

Assuming that your Oper-line and config file are in place, you can run mjolnir without any argument and merely call the compiled binary by itself. If your GOBIN is in your path, then the command would simply be `mjolnir`. If your config file is not named `mjolnir.yaml` and in the `$PWD`, then you would need to specify it at startup like so:

```
mjolnir --config alternative-config-file.yaml
```
