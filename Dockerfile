FROM golang:latest 
ADD . /usr/local/go/src/gitlab.com/secops-space/mjolnir
WORKDIR /usr/local/go/src/gitlab.com/secops-space/mjolnir
RUN ./build.sh
ENTRYPOINT ["/usr/local/go/bin/mjolnir"]

